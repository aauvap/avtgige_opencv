import time, sys

def listCameras(vimba, system):
    # list available cameras (after enabling discovery for GigE cameras)
    if system.GeVTLIsPresent:
        system.runFeatureCommand("GeVDiscoveryAllOnce")
        time.sleep(0.2)
    cameraIds = vimba.getCameraIds()
    if not cameraIds:
        print("No cameras available.")
        sys.exit()
    for idx, cameraId in enumerate(cameraIds):
        print("Cam ID [",idx,"] and name:",cameraId)
    return cameraIds

def openCamera(vimba,cameraIds):
    global camera
    # Prompt the user for the camera to use
    print("Which camera do you want to use? Default is [0]")
    camId = input()
    # Check if input is a valid number
    try:
        camId = int(camId)
    except ValueError:
        print("Default value chosen [0]")
        camId = 0
    if camId < 0 or camId >= len(cameraIds):
        print("Default value chosen [0]");
        camId = 0

    camera = vimba.getCamera(cameraIds[camId])
    camera.openCamera()
    print("")
    print("Cam",camId,"opened succesfully.")

    # Setup camera parameters
    camera.PixelFormat = 'Mono8'
    camera.AcquisitionFrameCount = 1
    camera.AcquisitionMode = 'Continuous'
    camera.BinningHorizontal = 1
    camera.BinningVertical = 1
    camera.AcquisitionFrameRateAbs = 1
    # ImageFormat
    camera.OffsetX = 0
    camera.OffsetY = 0
    camera.Height = 600
    camera.Width = 800
    # Acquisition->Trigger
    camera.TriggerSource = 'Freerun'
    camera.TriggerSelector = 'FrameStart'
    camera.TriggerActivation = 'RisingEdge'
    # EventControl
    camera.EventSelector = "AcquisitionStart"
    # GigE
    camera.BandwidthControlMode = "StreamBytesPerSecond"
    camera.GevSCPSPacketSize = 1518
    camera.StreamFrameRateConstrain = "true"

    return camera
