import time
import openCamera as c

def changeSettings():
    fancySleep = 0.05
    while (1):
        print("Settings:")
        time.sleep(fancySleep)
        print("[s] show current settings")
        time.sleep(fancySleep)
        print("[l] list all camera features")
        time.sleep(fancySleep)
        print("[c] change settings")
        time.sleep(fancySleep)
        print("\n[b] back\n")

        # Prompt user
        cmd = input()
        if any(x in cmd for x in ['back','b','B']):
            break;
        elif any(x in cmd for x in ['settings','s','S']):
            showSettings()
        elif any(x in cmd for x in ['listfeatures','l','L']):
            # List all camera features
            listCameraFeatures()    
        elif any(x in cmd for x in ['changefeatures','c','C']):
            setCameraFeature()
        else:
            print("Try again...")

def showMinimalSettings():
    print("___Camera settings___")
    print("Resolution:",c.camera.Width,"x",c.camera.Height)
    print("Color mode:",c.camera.PixelFormat)

def showSettings():
    print("___Camera settings___")
    cameraFeatureNames = c.camera.getFeatureNames()
    for name in cameraFeatureNames:
        try:
            print("{0}: {1}".format(name,getattr(c.camera,name)))
        except:
            continue
    print("_______\n") 
    time.sleep(1)

def resetSettings():
    c.camera.OffsetY = 0
    c.camera.OffsetX = 0

def listCameraFeatures():
    cameraFeatureNames = c.camera.getFeatureNames()
    for name in cameraFeatureNames:
        print(name)
    input("\nPress Enter for back...")
    print("_______\n")

def setCameraFeature():
    print("*** WARNING! ***")
    print("You should only change features if you know what you are doing!")
    print("Only minimal safety has been implemented..")
    print("****************")
    time.sleep(0.75)
    while (1):
        print("\nWhat feature do you want to change? (Empty for back)")
        cmd = input()
        
        camFeats = c.camera.getFeatureNames()
        if any(x in cmd for x in camFeats):
            print(cmd, ":", getattr(c.camera,cmd))
            print("Change to:")
            val = input()
            # If input is an integer or float, cast to the given type
            try:
                val = int(val)
            except ValueError:
                print("")

            try:
                setattr(c.camera,cmd,val)
                print(cmd,"=",val)
            except:
                print("[",cmd,"] cannot be set to [",val,"].") 

        elif cmd == None or cmd == "":
            break;
        else:
            print("Cannot set feature: [", cmd, "]")

def changeResolution(width,height):
    c.camera.Height = int(height)
    c.camera.Width = int(width)
    #c.camera.OffsetY = int((c.camera.HeightMax/2) - (c.camera.Height/2))
    #c.camera.OffsetX = int((c.camera.WidthMax/2) - (c.camera.Width/2))

def changeCameraSettings():
    yes = ["yes","Yes","y","Y"];

    # Return if no change is wanted
    print("Do you want to change the resolution? [no]/yes")
    cmd = input()
    if not any(x in cmd for x in yes):
        changeResolution(c.camera.Width,c.camera.Height)
        return
    width = 0
    height = 0
    while width == 0 or height == 0:
        print("Width:")
        width = input()
        try:
            width = int(width)
        except ValueError:
            print("Error - invalid value. Try again.");
            width = 0
            continue;
        print("Height:")
        height = input()
        try:
            height = int(height)
        except ValueError:
            print("Error - invalid value. Try again.");
            height = 0
            continue;
        if width < 1 or width > 1936:
            print("Error. Width must be in the interval [1-1936].")
            width = 0
        if height < 1 or height > 1216:
            print("Error. Height must be in the interval [1-1216].")
            height = 0 
    changeResolution(width,height);
    time.sleep(0.3)

def changeRecordingTime(capTime):
    yes = ["yes","Yes","y","Y"];
    time = capTime

    print("Do you want to change the RECORDING TIME? [no]/yes")
    cmd = input()
    if any(x in cmd for x in yes): 
        time = 0
        while time == 0:
            print("Rec. time:")
            time = input()
            try:
                time = int(time)
            except ValueError:
                print("Invalid value. Try again.")
                time = 0;
            if time < 1 or time > 1000:
                print("Time must be in the following interval [1-1000].")
                time = 0
    return time
