import time, cv2
import numpy as np
from changeSettings import changeCameraSettings, showMinimalSettings
import openCamera as c

def streamImages():
    # Setup standard variables
    n_vimba_frames = 10;

    # Notify the user about the camera settings
    showMinimalSettings()
    time.sleep(0.3)

    # Prompt the user for camera settings
    changeCameraSettings()
    time.sleep(1)

    # Create Vimba frames and add a callback to be run when a new frame is captured
    frame_pool = [c.camera.getFrame() for _ in range(n_vimba_frames)]
    for frame in frame_pool:
        frame.announceFrame()
        frame.queueFrameCapture(showFrameCallback)

    # Start capturing
    c.camera.startCapture()
    c.camera.runFeatureCommand('AcquisitionStart')
    # Wait for user input to shut down
    input("Press enter to shutdown...")
    # Stop acquisition
    c.camera.runFeatureCommand('AcquisitionStop')
    # Wait for the camera to stop properly
    time.sleep(1)
    # Wait for the user to shut window down - then clean up
    c.camera.flushCaptureQueue()
    c.camera.endCapture()
    c.camera.revokeAllFrames()
    cv2.destroyAllWindows()

def showFrameCallback(frame):
    # Get data from frame
    data = frame.getBufferByteData()  
    # Use numpy to collect images
    img = np.ndarray(buffer = data,
                     dtype = np.uint8,
                     shape = (frame.height, frame.width, 1))
    # Convert data to RGB image
    img = convertToRGB(img)
    # Show image on screen
    cv2.imshow('Stream',img)
    cv2.waitKey(1)

    # Queue the frame capturer with a recursive callback
    frame.queueFrameCapture(showFrameCallback)

def convertToRGB(img):
    img = cv2.cvtColor(img, cv2.COLOR_BAYER_RG2RGB)
    return img
