import time, pymba, os
from capture import captureImages
from stream import streamImages
from openCamera import openCamera, listCameras
from changeSettings import changeSettings

def main():
    fancySleep = 0.05
    # start Vimba
    with pymba.Vimba() as vimba:
        # get system object
        system = vimba.getSystem()
        
        cameraIds = listCameras(vimba,system); 
        camera = openCamera(vimba,cameraIds)
        
        capTime = 2
        while(1):
            # Show options
            time.sleep(0.2)
            print("\n*****************")
            print("Options:")
            time.sleep(fancySleep)
            print("[f] settings")
            time.sleep(fancySleep)
            print("[r] record")
            time.sleep(fancySleep)
            print("[s] stream")
            time.sleep(fancySleep)
            print("[c] change camera")
            time.sleep(fancySleep)
            print("\n[q] exit\n")
            
            # Get user input
            cmd = input()
            if any(x in cmd for x in ['exit','q','Q']):
                print("Shutting down.")
                break;
            elif any(x in cmd for x in ['settings','f','F']):
                changeSettings()
            elif any(x in cmd for x in ['record','r','R']):
                captureImages(capTime)
            elif any(x in cmd for x in ['stream','s','S']):
                streamImages()    
            elif any(x in cmd for x in ['change','c','C']):
                camera.closeCamera()
                camera = openCamera(vimba,cameraIds)
main()
