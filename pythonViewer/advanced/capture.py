import time, os, cv2
import numpy as np
from changeSettings import showMinimalSettings, changeCameraSettings, changeRecordingTime
import openCamera as c

def captureImages(capTime):
    # Setup standard variables
    global frameCounter
    frameCounter = 0
    n_vimba_frames = 10

    # Notify the user about the camera settings
    showMinimalSettings() 
    print("Recording time: {0}s".format(capTime))
    time.sleep(0.3)
    
    # Prompt the user for camera settings
    changeCameraSettings()
    time.sleep(0.3)

    # Prompt the user for recording time
    capTime = changeRecordingTime(capTime)

    totalFrames = int(capTime * c.camera.AcquisitionFrameRateLimit)

    # Create Vimba frames and add a callback to be run when a new frame is captured
    frame_pool = [c.camera.getFrame() for _ in range(n_vimba_frames)]
    for frame in frame_pool:
        frame.announceFrame()
        frame.queueFrameCapture(writeFrameCallback)

    print("***Start capturing frames***")
    print("Recording time: {0}s".format(capTime))
    print("Total frames:", totalFrames);

    # Start capturing
    c.camera.startCapture()
    c.camera.runFeatureCommand('AcquisitionStart')
    # Set recording time
    time.sleep(capTime)
    # Stop acquisition
    c.camera.runFeatureCommand('AcquisitionStop')
    # Wait for the camera to stop properly
    time.sleep(1)
    # Clean up
    c.camera.flushCaptureQueue()
    c.camera.endCapture()
    c.camera.revokeAllFrames()
    print("\n...Done")

def writeFrameCallback(frame):
    global videoLogger, frameCounter
    directory = 'imgs' + str(frame.width) + 'x'+ str(frame.height) + '/'

    # Check if folder exists, otherwise make it
    if not os.path.isdir(directory):
        os.mkdir(directory)

    print("Storing frames in {0}. Processing {1}".format(directory, frameCounter),end="\r")

    data = frame.getBufferByteData()

    # Use numpy to collect images
    img = np.ndarray(buffer = data,
                         dtype = np.uint8,
                         shape = (frame.height, frame.width, 1))
    img = cv2.cvtColor(img, cv2.COLOR_BAYER_RG2RGB)

    # Save image
    cv2.imwrite(directory + 'img' + str(frameCounter) + '.png', img)
    
    # Queue the frame capturer with a recursive callback
    frame.queueFrameCapture(writeFrameCallback)
    
    # Increase counter
    frameCounter += 1
