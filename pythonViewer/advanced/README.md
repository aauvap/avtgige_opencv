# README
This is a small program, that allows the user to access most features on the camera through the command-line.

# RUNNING THE PROGRAM
```$python viewer.py```

# WARNING
Use of this program is on your own risk. 
It has not been thoroughly tested, so be cautious and be sure that you know what you are doing.