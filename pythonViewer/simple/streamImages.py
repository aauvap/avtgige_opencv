import time, pymba, os, cv2, sys
import numpy as np

def listCameras(vimba, system):
    # list available cameras (after enabling discovery for GigE cameras)
    if system.GeVTLIsPresent:
        system.runFeatureCommand("GeVDiscoveryAllOnce")
        time.sleep(0.2)
    cameraIds = vimba.getCameraIds()
    if not cameraIds:
        print("No cameras available.")
        sys.exit()
    for idx, cameraId in enumerate(cameraIds):
        print("Cam ID [",idx,"] and name:",cameraId)
    return cameraIds

def openCamera(vimba,cameraIds):
    global camera
    # Prompt the user for the camera to use
    print("Which camera do you want to use? Default is [0]")
    camId = input()
    # Check if input is a valid number
    try:
        camId = int(camId)
    except ValueError:
        print("Input is not a valid number. Default value chosen [0]")
        camId = 0
    if camId < 0 or camId >= len(cameraIds):
        print("Camera ID is not valid. Default value chosen [0]");
        camId = 0

    camera = vimba.getCamera(cameraIds[camId])
    camera.openCamera()
    print("")
    print("Cam",camId,"opened succesfully.")

    # Setup camera parameters
    camera.PixelFormat = 'Mono8' # Formats: [Mono8][BayerRG8][BayerRG12][BayerRG12Packed][RGB8Packed][BGR8Packed][YUV411Packed][YUV422Packed][YUV444Packed]
    camera.AcquisitionFrameCount = 1
    camera.AcquisitionMode = 'Continuous'
    camera.BinningHorizontal = 1
    camera.BinningVertical = 1
    camera.AcquisitionFrameRateAbs = 1
    # Acquisition->Trigger
    camera.TriggerSource = 'Freerun'
    camera.TriggerSelector = 'FrameStart'
    camera.TriggerActivation = 'RisingEdge'
    # EventControl
    camera.EventSelector = "AcquisitionStart"
    # GigE
    camera.BandwidthControlMode = "StreamBytesPerSecond"
    camera.GevSCPSPacketSize = 1518
    camera.StreamFrameRateConstrain = "true"

    return camera

def streamImages(camera):
    # Setup standard variables
    n_vimba_frames = 10;

    # Create Vimba frames and add a callback to be run when a new frame is captured
    frame_pool = [camera.getFrame() for _ in range(n_vimba_frames)]
    for frame in frame_pool:
        frame.announceFrame()
        frame.queueFrameCapture(showFrameCallback)

    # Start capturing
    camera.startCapture()
    camera.runFeatureCommand('AcquisitionStart')
    # Wait for user input to shut down
    input("Press enter to shutdown...")
    # Stop acquisition
    camera.runFeatureCommand('AcquisitionStop')
    # Wait for the camera to stop properly
    time.sleep(1)
    # Wait for the user to shut window down - then clean up
    camera.flushCaptureQueue()
    camera.endCapture()
    camera.revokeAllFrames()
    cv2.destroyAllWindows()

def showFrameCallback(frame):
    # Get data from frame
    data = frame.getBufferByteData()  
    # Use numpy to collect images
    img = np.ndarray(buffer = data,
                     dtype = np.uint8,
                     shape = (frame.height, frame.width, 1))
    # Convert data to RGB image
    img = cv2.cvtColor(img, cv2.COLOR_BAYER_RG2RGB)
    # Show image on screen
    cv2.imshow('Stream',img)
    cv2.waitKey(1)

    # Queue the frame capturer with a recursive callback
    frame.queueFrameCapture(showFrameCallback)

############### SCRIPT PART ####################
with pymba.Vimba() as vimba:
    # get system object
    system = vimba.getSystem()
    
    cameraIds = listCameras(vimba,system); 
    camera = openCamera(vimba,cameraIds)

    # stream
    streamImages(camera)
