# README
This repo is for guides and scripts on how to use OpenCV to process images from AVT GigE cameras, such as Mako.

# Installation Guide (LINUX) - tested on Ubuntu 16.04
1. Make sure that you have the following packets installed
    - Numpy
    - OpenCV 3.x

2. Download and install Vimba SDK from AVT to default directory
    - Can be downloaded from https://www.alliedvision.com/fileadmin/content/software/software/Vimba/Vimba_v2.1_Linux.tgz

    Vimba comes as a tarball. To install it, follow these steps:
    1. Uncompress the archive with the command tar -xf ./Vimba.tgz to a directory you have writing
       privileges for, e.g.: /opt: tar -xzf ./Vimba.tgz -C /opt
       Under this directory, Vimba will be installed in its own folder. We refer to this path as [InstallDir].

    2. GigE camera users: Go to [InstallDir]/Vimba_x_x/VimbaGigETL. USB camera users: Go to [InstallDir]/Vimba_x_x/VimbaUSBTL.

    3. Execute the shell script Install.sh with root privileges (e.g., sudo ./Install.sh or su -c.Install.sh). 
       If you use GigE and USB cameras, perform this step for both TLs (transport layers).

    4. Restart the computer.

    - See more at: https://www.alliedvision.com/fileadmin/content/documents/products/software/software/Vimba/appnote/Vimba_installation_under_Linux.pdf

3. Download and install pymba
    1. Download pymba from https://github.com/morefigs/pymba
    2. Run $python setup.py install
    
    3. Open python and run the following script to verify that the installation has succeeded:

            from pymba import Vimba

            print(Vimba.version())

    - TROUBLESHOOTING
    - If pymba causes a problem with the GENICAM_GENTLXX_PATH, have a look in the pymba/vimbadll.py file.
        - The "vimbaC_path" may be pointing to the wrong location.
        - Correct location for x86_64: foo/bar/Vimba_2_1/VimbaC/DynamicLib/x86_64bit/libVimbaC.so
        - After changing the path run $python setup.py install

4. Restart the computer

5. Change IPv4 settings for the ethernet card connected to the PoE Switch/Camera.
    - IP: 169.254.100.1
    - Netmask: 255.255.0.0
    - Gateway: (empty)

**Note**: you may need to restart your computer once more in order to make the IP change.