from time import sleep
from pymba import Vimba
import cv2, argparse, sys

def display_frame(frame):
    """ 
    Display the acquired frame

    frame: The frame object to display
    """
    img = frame.buffer_data_numpy()

    try:
        cv2.imshow('Image',img)
    except:
        pass
    cv2.waitKey(1)

def setup_camera(camera):
    """
    Adjust the camera settings such as exposure time, gain, binning, etc.
 
    """

    # Setup camera parameters
    camera.ExposureAutoTarget = 40
    camera.BlackLevel = 4
    camera.ExposureTimeAbs = 20000 # Exposure time in micro seconds
    camera.Gain = 14
    camera.PixelFormat = 'Mono8' # Formats: [Mono8][BayerRG8][BayerRG12][BayerRG12Packed][RGB8Packed][BGR8Packed][YUV411Packed][YUV422Packed][YUV444Packed]
    camera.AcquisitionFrameCount = 1
    camera.AcquisitionMode = 'Continuous'
    camera.BinningHorizontal = 1
    camera.BinningVertical = 1
    camera.AcquisitionFrameRateAbs = 1
    # Acquisition->Trigger
    camera.TriggerSource = 'Freerun'
    camera.TriggerSelector = 'FrameStart'
    camera.TriggerActivation = 'RisingEdge'
    # EventControl
    camera.EventSelector = "AcquisitionStart"
    # GigE
    camera.BandwidthControlMode = "StreamBytesPerSecond"
    camera.StreamFrameRateConstrain = "true"

if __name__ == '__main__':
    # construct the argument parser and parse the arguments
    ap = argparse.ArgumentParser()
    ap.add_argument("-t", "--time", help="Adjust recording time in seconds", type=int, default=5)

    args = vars(ap.parse_args())
    recTime = int(args["time"])

    with Vimba() as vimba:

        # Open camera
        try:
            camera = vimba.camera(0)
            camera.open()
        except:
            print("No camera found. Exitting.")
            sys.exit()

        camera.arm('Continuous',display_frame)
        setup_camera(camera)
        camera.start_frame_acquisition()

        # Adjust recording time
        sleep(recTime)
   
        # Shut down any OpenCV windows
        cv2.destroyAllWindows()

        # Close camera
        camera.disarm()
        camera.close()
